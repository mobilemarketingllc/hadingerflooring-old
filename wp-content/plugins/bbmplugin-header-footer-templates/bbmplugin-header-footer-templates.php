<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/*
Plugin Name: BBPlugin - Header Footer Templates
Plugin URI: https://j7digital.com
Description: Adds a panel to the Theme customizer to insert a layout as a header/footer globally on each page.
Author: J7 Digital
Author URI: https://j7digital.com
Version: 2.4
*/

define( 'bbp_hft_version', '2.4' );

##########################################################################################
//Load Updater
##########################################################################################
##########################################################################################
##########################################################################################


define( 'bbp_hft_store_url', 'https://j7digital.com/' );
define( 'bbp_hft_item_name', 'BBPlugin - Header Footer Templates' );
register_deactivation_hook( __FILE__, 'bbp_hft_plugin_deactivate' );


if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
	include( dirname( __FILE__ ) . '/plugin/includes/EDD_SL_Plugin_Updater.php' );
}
add_action( 'admin_init', 'bbp_hft_plugin_updater', 10 );

function bbp_hft_plugin_updater() {
	$license_key = trim( get_option( 'bbp_hft_license_key' ) );
	$edd_updater = new EDD_SL_Plugin_Updater( bbp_hft_store_url, __FILE__, array(
		'version' 	=> bbp_hft_version,
		'license' 	=> $license_key,
		'item_name' => bbp_hft_item_name,
		'author' 	=> 'J7 Digital',
		'url'		=> home_url(),
		)
	);
}
include( dirname( __FILE__ ) . '/plugin/includes/bbp_hft_plugin_updater.php' );
add_action( 'init', 'bbp_hft_load_init' );

##########################################################################################
//Load Plugin
##########################################################################################
##########################################################################################
##########################################################################################
##########################################################################################


function bbp_hft_load_init() {
	//checks for BB-plugin
	if ( class_exists( 'FLBuilder' ) )
	{
		//checks for BB-theme
		$theme = wp_get_theme();
		if ('Beaver Builder Theme' == $theme->name || 'Beaver Builder Theme' == $theme->parent_theme)
		{
			require_once 'plugin/hft-header-footer-templates-functions.php';
		}
		else
		{
			add_action( 'admin_head', 'bbp_hft_no_bb_theme');
		}

	//check for update licesnse
		$license 	= get_option( 'bbp_hft_license_key' );
		$status 	= get_option( 'bbp_hft_license_status' );
		$bbp_hft_license_status_info = get_option( 'bbp_hft_license_status_info' );
		if ($license != false && $status == 'valid' && $status !== 'false')
		{
			return;
		}
		else
		{
			add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'bbp_hft_actionlinks' );
		}
	}
}


##########################################################################################
//Load Init Plugin Functions
##########################################################################################
##########################################################################################
##########################################################################################

function bbp_hft_actionlinks( $links ) {
	$links[] = '<a style="border: 1px solid green; border-radius: 10px; padding: 0px 15px;" href="'. esc_url( get_admin_url(null, 'options-general.php?page=bbp_hft_license') ) .'">Enter Updater License</a>';
	return $links;
}



function bbp_hft_plugin_deactivate()
{
	if ( ! current_user_can( 'activate_plugins' ) )
	{
		return;
	}
	delete_option('bbp_hft_license_key');
	delete_option('bbp_hft_license_status');
	delete_option('bbp_hft_license_status_info');
	delete_option('bbp_hft_license_status_authsite');

}

// Adds an alert to plugin page if Theme is missing.
function bbp_hft_no_bb_theme()
{
	?>
	<script type="text/javascript">
		(function($) {
			$(document).ready(function() {
				$('[data-slug=bbplugin-header-footer-templates] td.column-description.desc').append('<div style="background-color: #AB6868; color: #ffffff; padding: 5px 20px; font-weight: 900;">This Plugin Requires the Premium Beaver Builder Theme.</div>');

			});
		})(jQuery);
	</script>
	<?php
}
