var $ = jQuery;

$(window).ready(function() {

    //CUSTOM MODAL
    $("body").on("click", ".open_custom_modal", function(e) {
        e.preventDefault();
        var modal = $($(this).attr("href"));
        if (modal.length) {
            $(".custom_modal_open").remove();
            $("body").append("<div class='custom_modal custom_modal_open'>" + modal.html() + "</div>");
            var open_modal = $(".custom_modal_open");
            open_modal.show();
            console.log($(window).scrollTop(), $(window).height(), modal.find(".modal_body").outerHeight());
            open_modal.css("top", $(window).scrollTop() + ($(window).height() - open_modal.find(".modal_body").outerHeight()) / 2);
        }
    });

    $("body").on("click", ".custom_modal .close_modal", function(e) {
        e.preventDefault();
        $(".custom_modal_open").remove();
    });


    fr_add_filter("fr_slider_init", function(settings, slider) {
        if ($(window).width() < 726) {
            if (slider.is(".fr-slider")) {
                settings.slidesToScroll = 2;
                settings.slidesToShow = 2;
            }
        }

        if (slider.parent().is(".fl-logo-slider")) {
            if ($(window).width() < 726) {
                settings.slidesToScroll = 3;
                settings.slidesToShow = 3;
            }
            if ($(window).width() < 450) {
                settings.slidesToScroll = 2;
                settings.slidesToShow = 2;
            }
        }
        return settings;
    });

    //SLIDER
    //Example: {"slidesToScroll":7,"slidesToShow":7,"arrows":false,"infinite": false}
    //More options can be found here: http://kenwheeler.github.io/slick/
    if ($().slick) {
        fr_slider_init(".fr-slider");

        $("body").on("click", ".fr-slider .prev", function(e) {
            e.preventDefault();
            $(this).parents(".fr-slider").first().find(".slides").slick('slickPrev');
        });
        $("body").on("click", ".fr-slider .next", function(e) {
            e.preventDefault();
            $(this).parents(".fr-slider").first().find(".slides").slick('slickNext');
        });
    }

    function fr_slider_init(ob) {
        $(ob).each(function() {
            var default_settings = { slidesToScroll: 1, slidesToShow: 1 }
            var settings = fr_parse_attr_data($(this).attr("data-fr"));
            settings = $.extend(default_settings, settings);
            settings = fr_apply_filter("fr_slider_init", settings, [$(this)]);
            console.log(settings);
            $(this).find(".slides").slick(settings);


            $(this).find(".slides").on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                next_slide(this, nextSlide);
            });

            function next_slide(ob, nextSlide) {
                if (nextSlide == 0) {
                    $(ob).parents(".fr-slider").first().find(".arrow.prev").animate({ opacity: 0 });
                } else {
                    $(ob).parents(".fr-slider").first().find(".arrow.prev").animate({ opacity: 1 });
                }

                if (nextSlide >= $(ob).find(".slide").length - settings.slidesToScroll) {
                    $(ob).parents(".fr-slider").first().find(".arrow.next").animate({ opacity: 0 });
                } else {
                    $(ob).parents(".fr-slider").first().find(".arrow.next").animate({ opacity: 1 });
                }
            }

            next_slide($(this).find(".slides"), 0);

            $(this).addClass("init");
        });
    }

    function fr_slider_delete(ob) {
        $(ob).each(function() {
            $(this).find(".slides").slick('unslick');
        });
    }




    //REPLACERS
    $("body").on("click", "[data-fr-replace-src]", function(e) {
        e.preventDefault();
        $($(this).attr("data-fr-replace-src")).attr("src", $(this).attr("data-src"));
    });

    $("body").on("click", "[data-fr-replace-bg]", function(e) {
        e.preventDefault();
        $($(this).attr("data-fr-replace-bg")).css("background-image", "url(" + $(this).attr("data-background") + ")");
    });



    //TOGGLE BOX CONTENT
    $(".fr_toggle_box .box_content").each(function() {
        $(this).show();
        var dir = $(this).parents(".fr_toggle_box").first().data("dir");
        var size = $(this).outerHeight();
        if (dir == "left" || dir == "right") {
            size = $(this).outerWidth();
        }
        $(this).css(dir, -size);
    });

    $(".fr_toggle_box .handle").click(function() {
        var parent = $(this).parents(".fr_toggle_box").first();
        var content = parent.find(".box_content").first();
        var dir = parent.data("dir");
        if (!dir) {
            dir = "bottom";
        }
        data = {};
        if (parent.is(".active")) { //IS OPEN
            var size = content.outerHeight();
            if (dir == "left" || dir == "right") {
                size = content.outerWidth();
            }
            data[dir] = -size;
            content.stop().animate(data);
            parent.removeClass("active");
            parent.find(".bg").fadeOut();
        } else {
            data[dir] = 0;
            content.stop().animate(data);
            parent.addClass("active");
            parent.find(".bg").fadeIn();
        }
    });

    $(window).load(function() {
        setTimeout(function() {
            if (!$.cookie('fr_toggle_box_opened')) {
                $(".fr_toggle_box .handle").click();
                setTimeout(function() {
                    if ($(".fr_toggle_box").is(".active")) {
                        $(".fr_toggle_box .handle").click();
                    }
                }, 7000);
                $.cookie('fr_toggle_box_opened', 1, { expires: 365, path: '/' });
            }
        }, 1000);
    });



    var fr_link_dont_redirect = 0;
    $("body").on("click", "[data-fr-link] a", function(e) {
        fr_link_dont_redirect++;
    });

    $("body").on("click", "[data-fr-link]", function(e) {
        if (fr_link_dont_redirect <= 0) {
            window.location = $(this).attr("data-fr-link");
            return false;
        } else {
            fr_link_dont_redirect--;
        }
    });



    //SLIDER MENU
    $(".slider-menu").each(function() {
        $(this).height($(this).parent().height());
    });

    fr_click_outside(".slider-menu", ".slider-menu", function(ob) {
        return $(ob).is(".active");
    }, function(ob) {
        if ($(ob).is(".active")) {
            close_slider_menu(ob);
        } else {
            open_slider_menu(ob);
        }
    });

    $(".slider-menu").find(".icon").click(function() {
        if (!$(this).parents(".slider-menu").is(".active")) {
            open_slider_menu($(this).parents(".slider-menu"));
        } else {
            close_slider_menu($(this).parents(".slider-menu"));
        }
    });

    function open_slider_menu(ob) {
        $(ob).addClass("active");
        $(ob).find("ul").stop().animate({ "margin-right": 0 });
    }

    function close_slider_menu(ob) {
        $(ob).removeClass("active");
        $(ob).find("ul").stop().animate({ "margin-right": -$(ob).outerWidth() + $(ob).find(".icon").first().outerWidth() });
    }

    $("body").on("click", "a[href^='#']", function(e) {
        var target = $(this).attr("href");
        if ($(target).length && $(target).is(".fr_popup")) {
            e.preventDefault();
            if (!$(target).is(".active")) {
                fr_open_popup(target);
            }
        }
    });

    $("body").on("click", ".fr_popup .close_popup", function(e) {
        e.preventDefault();
        var target = $(this).parents(".fr_popup");
        if ($(target).is(".active")) {
            fr_close_popup(target);
        }
    });

    function fr_open_popup(target) {
        $(target).show();
        fr_center_in_window($(target).find(".content"));
        $(target).addClass("active");
    }

    function fr_close_popup(target) {
        $(target).removeClass("active");
        $(target).hide();
    }

    function fr_center_in_window(ob, offset) {
        if (!offset) {
            offset = 0;
        }
        var wt = $(window).scrollTop();
        var wh = $(window).height();
        var oh = $(ob).outerHeight();
        console.log(wh, oh, wt, offset);
        $(ob).css("top", (wh - oh) / 2 + wt + offset);
    }



    $("body").on("click", ".open-gallery-modal", function(e) {
        e.preventDefault();

        if (!$(".active-gallery-modal").length) {
            $(".fl-page-content").prepend("<div class='active-gallery-modal'></div>");
        }

        var html = $(this).find(".gallery-modal").html();
        if ($(".active-gallery-modal").is(":visible")) {
            close_modal(function() {
                open_modal(html);
            });
        } else {
            open_modal(html);
        }
    });

    $("body").on("click", ".active-gallery-modal .close_modal", function(e) {
        e.preventDefault();
        close_modal();
    });

    //$(".open-gallery-modal").first().click();

    function close_modal(func) {
        $(".active-gallery-modal").fadeOut(func);
    }

    function open_modal(html) {
        var header = $(".fl-page-header").outerHeight();
        if (!$(".fl-page-header").is(":visible")) {
            header = $("#djcustom-header").outerHeight();
        }
        console.log(header);
        $(".active-gallery-modal").css("top", header + $(window).scrollTop());
        $(".active-gallery-modal").html(html);
        $(".active-gallery-modal").fadeIn();
    }


    var hash = window.location.hash.substr(1);

    $(".fl-tabs-label").each(function() {
        if ($.trim($(this).text()).toLowerCase() == hash.toLowerCase()) {
            console.log($(this));
            change_tab($(this));
            return false;
        }
    });


    $("footer .menu-item-has-children").click(function(e) {
        if ($(window).width() <= 768) {
            e.preventDefault();
            $(this).find(">ul").toggle();
        }
    });

    //$("header button.navbar-toggle").before("<a class='show_menu' href='#'><i class='fa fa-bars'></i></a>");
    //$("header button.navbar-toggle").before("<a class='close_search' href='#'><i class='fa fa-close'></i></a>");

    $("header#djcustom-header").append("<div class='search_wrapper'>" + $(".fl-page-nav-search").html() + "</div>");
    $("header#djcustom-header .search_wrapper>a").click(function(e) {
        e.preventDefault();
        $(this).next().submit();
    });

    $("header .fl-photo").before("<a href='#' class='search_mobile'><i class='fa fa-search'></i></a>");


    $("header .search_mobile").click(function(e) {
        e.preventDefault();
        $("header .search_wrapper").slideToggle();
        $("header .close_search").click();
    });



    $(".show_menu").click(function(e) {
        e.preventDefault();
        $(".sc_mega_menu").fadeIn();
        $(".sc_mega_menu").addClass("open_menu");
        $("header .sc_mega_menu .mega-menu>ul").each(function() {
            $(this).parent().addClass("has_subitems");
            $(".sc_mega_menu .fl-page-nav-wrap .secondary_menu").remove();
            $(".sc_mega_menu .fl-page-nav-wrap").prepend("<ul class='secondary_menu nav navbar-nav menu'></ul>");
            $("header .show_menu").hide();
            $("header .close_search").show();

            var menu = $(this).parents(".sc_mega_menu .fl-page-nav-wrap");

            $("header .close_search,.sc_mega_menu .sc_overlay").click(function(e) {
                e.preventDefault();
                $(".sc_mega_menu").fadeOut();
                $("header .show_menu").show();
                $("header .close_search").hide();
                $(".sc_mega_menu .main_link>a").click();
                $(".sc_mega_menu").removeClass("open_menu");
            });

            $(this).parent().unbind().click(function(e) {
                var ob = $(this);
                e.preventDefault();
                menu.animate({ "margin-left": -menu.outerWidth() }, function() {
                    if (!$(".sc_mega_menu").is(".active")) {
                        $(".sc_mega_menu .secondary_menu").html("<li class='main_link'>" + $(ob).html() + "</li>");
                        $(".sc_mega_menu").addClass("active");

                        $(".main_link>a").click(function(e) {
                            e.preventDefault();
                            menu.animate({ "margin-left": -menu.outerWidth() }, function() {
                                $(".sc_mega_menu .secondary_menu").html("");
                                $(".sc_mega_menu").removeClass("active");
                                menu.animate({ "margin-left": 0 });
                            });
                        });
                    } else {
                        $(".sc_mega_menu").removeClass("active");
                    }
                    menu.animate({ "margin-left": 0 });
                });
            });
        });
        $(window).resize();
    });

    $(window).resize(function() {
        if ($(".sc_mega_menu").is(".open_menu")) {
            var h = $("body").height() - $("header").outerHeight() + 20;
            $(".sc_mega_menu .sc_overlay,.sc_mega_menu .fl-page-nav-wrap").height(h);
        }
    });
    $(window).resize();

    $(window).scroll(function() {
        $(window).resize();
    });
});

function fr_click_outside(excluded_element, element_to_hide, cond_func, hide_func) {
    $(document).click(function(event) {
        if (!$(event.target).closest(excluded_element).length) {
            if ((cond_func && cond_func(element_to_hide)) || (!cond_func && $(element_to_hide).is(":visible"))) {
                if (hide_func) {
                    hide_func(element_to_hide);
                } else {
                    $(element_to_hide).hide();
                }
            }
        }
    });
}

function change_tab(ob) {
    ob.parent().find(".fl-tab-active").removeClass("fl-tab-active");
    ob.addClass("fl-tab-active");
    var index = ob.index();

    ob.parents(".fl-tabs").first().find(".fl-tabs-panel .fl-tabs-label").removeClass("fl-tab-active");
    ob.parents(".fl-tabs").first().find(".fl-tabs-panel .fl-tabs-panel-content").removeClass("fl-tab-active");
    ob.parents(".fl-tabs").first().find(".fl-tabs-panel:eq(" + index + ") .fl-tabs-panel-content").addClass("fl-tab-active");
}







function fr_parse_attr_data(data) {
    if (!data) {
        data = "";
    }
    if (data.substr(0, 1) != "{") {
        data = "{" + data + "}";
    }
    return $.parseJSON(data);
}

//FILTER SYSTEM LIKE IN WORDPRESS
var fr_filters = [];

function fr_add_filter(filter, func) {
    filter = filter;
    if (typeof fr_filters[filter] == "undefined") {
        fr_filters[filter] = [];
    }
    fr_filters[filter][fr_filters[filter].length] = func;
}

function fr_apply_filter(filter, res, args) {
    if (typeof fr_filters[filter] != "undefined") {
        for (k in fr_filters[filter]) {
            var args2 = [res].concat(args);
            res = fr_filters[filter][k].apply(null, args2);
        }
    }
    return res;
}


(function($) {
    $(function() {
        FWP.loading_handler = function() {
            $(".facet_filters .close_sidebar").click(function(e) {
                e.preventDefault();
                $(".facet_filters").animate({ "left": -$(".facet_filters").outerWidth() - 20 }, 500);
            });
            $(".open_sidebar").click(function(e) {
                e.preventDefault();
                $(".facet_filters").css("left", -$(".facet_filters").outerWidth() - 20);
                $(".facet_filters").animate({ "left": 0 }, 500);
            });
        }
    });

    jQuery(document).ready(function() {

        setTimeout(function() {

            jQuery(".facetwp-facet-brand").on('click', function(e) {
                jQuery(".brand_bgimg").hide();
                setTimeout(function() {
                    if (window.location.href.indexOf("fwp_brand=masland") > -1) {
                        jQuery("#masland_bgimg").show();
                    } else if (window.location.href.indexOf("fwp_brand=karastan") > -1) {
                        jQuery("#karastan_bgimg").show();
                    } else if (window.location.href.indexOf("fwp_brand=dixie-home") > -1) {
                        jQuery("#dixie_bgimg").show();
                    } else {
                        jQuery(".brand_bgimg").hide();
                    }
                }, 500);

            });
        }, 500);

        if (window.location.href.indexOf("fwp_brand=masland") > -1) {
            jQuery("#masland_bgimg").show();
        } else if (window.location.href.indexOf("fwp_brand=karastan") > -1) {
            jQuery("#karastan_bgimg").show();
        } else if (window.location.href.indexOf("fwp_brand=dixie-home") > -1) {
            jQuery("#dixie_bgimg").show();
        } else {
            jQuery(".brand_bgimg").hide();
        }
    });

})(jQuery);