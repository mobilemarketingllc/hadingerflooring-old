
<header class="fl-page-header fl-page-header-primary<?php FLTheme::header_classes(); ?>"<?php FLTheme::header_data_attrs(); ?> itemscope="itemscope" itemtype="http://schema.org/WPHeader">
	<div class="fl-page-header-wrap">
		<div class="fl-page-header-container container">
			<div class="fl-page-header-row row">

				<a href="#" class="search_mobile">
					<i class="fa fa-search"></i>
				</a>

				<a href="#" class="navbar-toggle ">
					<span class="show_menu"><?php FLTheme::nav_toggle_text(); ?></span>
					<span class="close_search"><i class='fa fa-close'></i></span>
				</a>
				<div class="col-md-2 col-sm-12 fl-page-header-logo-col">
					<div class="fl-page-header-logo" itemscope="itemscope" itemtype="http://schema.org/Organization">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" itemprop="url">


                            <?php
                            if (is_page( 11 )){ ?>
                                <img class="fl-logo-img" itemscope="" itemtype="http://schema.org/ImageObject" src="https://hadingerflooring.com/wp-content/uploads/2017/08/logo-hadinger-cabinets.png" data-retina="https://hadingerflooring.com/wp-content/uploads/2017/08/logo-hadinger-cabinets.png" alt="Hadinger Flooring">
                           <?php } elseif(is_page( 10 )) { ?>
                                <img class="fl-logo-img" itemscope="" itemtype="http://schema.org/ImageObject" src="https://hadingerflooring.com/wp-content/uploads/2017/08/logo-hadinger-rug.jpg" data-retina="https://hadingerflooring.com/wp-content/uploads/2017/08/logo-hadinger-rug.jpg" alt="Hadinger Flooring">
                           <?php } elseif(is_page( 32 )) {  ?>
                                <img class="fl-logo-img" itemscope="" itemtype="http://schema.org/ImageObject" src="https://hadingerflooring.com/wp-content/uploads/2018/08/HadingerCommercial-LOGO.jpg" alt="Hadinger Flooring">
                            <?php  } else { ?>
                            <?php FLTheme::logo(); ?>
                            <?php }  ?>

                        </a>
					</div>
				</div>
				<div class="fl-page-nav-col col-md-10 col-sm-12">
					<div class="fl-page-nav-wrap">
						<nav class="fl-page-nav fl-nav navbar navbar-default" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
							<div class="fl-page-nav-collapse collapse navbar-collapse">
								<?php
								FLTheme::nav_search();
								?>
								<?php echo do_shortcode('[mega_menu menu="Main Menu"]'); ?>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
	<form method="get" class="search_wrapper" role="search" action="<?php bloginfo("url") ?>" title="Type and press Enter to search.">
		<input type="text" class="fl-search-input form-control" name="s" placeholder="Search" value="">
	</form>
</header><!-- .fl-page-header -->