<div <?php post_class('fl-post-grid-post open-gallery-modal'); ?> itemscope itemtype="<?php FLPostGridModule::schema_itemtype(); ?>">
	
	<?php FLPostGridModule::schema_meta(); ?>

	<?php if(has_post_thumbnail() && $settings->show_image) : ?>
	<div class="fl-post-grid-image">
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			<?php the_post_thumbnail($settings->image_size); ?>
		</a>
	</div>
	<?php endif; ?>
	
	<?php 
	$image = get_field('room_scene_image_link');
	if(!$image){
		$image=get_field('swatch_image_link');
	}
	?>

	<div class="fl-post-grid-gallery" style="background: url('<?php echo $image ?>') no-repeat;background-size: cover;">


	</div>

	<div class="gallery-modal">
		<div class="fl-row-content-wrap">
			<div class="content">
				<div class="col col-sm-6 col-md-8 col-lg-8 col_swatch" style="background-image:url(<?php the_field("swatch_image_link") ?>)">
					&nbsp;
				</div>
				<div class="col col-sm-6 col-md-4 col-lg-4 product-box">
					<a href="#" class="close_modal"><i class="fa fa-times"></i></a>
	                <h2 class="collection"><?php the_field('collection'); ?></h2>
	                <h1 class="fl-post-title" itemprop="name">
	                   <?php the_field('color'); ?>
	                </h1>

	                <div class="product-colors">
	                    <?php
	                    $familysku = get_post_meta($post->ID, 'collection', true);
	                    if(is_singular( 'laminate' )){
	                        $flooringtype = 'laminate';
	                    } elseif(is_singular( 'hardwood' )){
	                        $flooringtype = 'hardwood';
	                    } elseif(is_singular( 'carpeting' )){
	                        $flooringtype = 'carpeting';
	                    } elseif(is_singular( 'luxury_vinyl_tile' )){
	                        $flooringtype = 'luxury_vinyl_tile';
	                    } elseif(is_singular( 'vinyl' )){
	                        $flooringtype = 'vinyl';
	                    } elseif(is_singular( 'solid_wpc_waterproof' )){
	                        $flooringtype = 'solid_wpc_waterproof';
	                    } elseif(is_singular( 'tile' )){
                            $flooringtype = 'tile';
                        } ?>


                        <?php if (is_page( 19677 )) { ?>
                            <?php
                            $args = array(
                                'post_type'      => array( 'carpeting', 'hardwood', 'laminate', 'luxury_vinyl_tile', 'vinyl', 'tile', 'solid_wpc_waterproof'  ),
                                'posts_per_page' => -1,
                                'post_status'    => 'publish',
                                'meta_query' => array(
                                    array(
                                        'key' => 'in_stock',
                                        'compare' => '==',
                                        'value' => '1'
                                    ))
                            );
                            ?>

                       <?php  } else { ?>
                            <?php
                            $args = array(
                                'post_type'      => $flooringtype,
                                'posts_per_page' => -1,
                                'post_status'    => 'publish',
                                'meta_query'     => array(
                                    array(
                                        'key'     => 'collection',
                                        'value'   => $familysku,
                                        'compare' => '='
                                    )
                                )
                            );
                            ?>

                       <?php } ?>







	                    <?php
	                    $the_query = new WP_Query( $args );
	                    ?>
	                    <?php  //echo $the_query ->found_posts; ?> <!-- Colors Available -->
	                </div>
                    <br>
                    <a href="<?php echo site_url();?>/coupon" class="coupon fl-button btn-gold"><span class="fl-button-text"><?php _e("GET COUPON","fl-builder"); ?></span></a>
					<br>
	                <a href="<?php the_permalink() ?>" class="view_more fl-button btn-white"><span class="fl-button-text"><?php _e("VIEW PRODUCT","fl-builder"); ?></span></a>
					<br>
	                <a href="<?php echo site_url();?>/financing" class="btn-link"><?php _e("REQUEST FINANCING >","fl-builder"); ?></a>
				</div>
			</div>
		</div>
	</div>

</div>