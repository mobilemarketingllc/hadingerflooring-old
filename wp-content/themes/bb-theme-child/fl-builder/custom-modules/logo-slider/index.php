<?php

/**
 * @class FLLogoSliderModule
 */
class FLLogoSliderModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('Logo Slider', 'fl-builder'),
			'description'   	=> __('Display a slider with logos.', 'fl-builder'),
			'category'      	=> __('Advanced Modules', 'fl-builder'),
			'editor_export' 	=> false,
			'partial_refresh'	=> true
		));
	}

	/**
	 * @method enqueue_scripts
	 */
	public function enqueue_scripts()
	{
		
	}

	/**
	 * Renders the schema structured data for the current
	 * post in the loop.
	 *
	 * @since 1.7.4
	 * @return void
	 */
	static public function schema_meta()
	{
		
	}

	/**
	 * Renders the schema itemtype for the current
	 * post in the loop.
	 *
	 * @since 1.7.4
	 * @return void
	 */
	static public function schema_itemtype()
	{
		
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('FLLogoSliderModule', array(
	'general'       => array(
		'title'         => __('General', 'fl-builder'),
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(
					'photos'        => array(
						'type'          => 'multiple-photos',
						'label'         => __('Photos', 'fl-builder'),
						'connections'   => array( 'multiple-photos' )
					)
				)
			)
		)
	)
));