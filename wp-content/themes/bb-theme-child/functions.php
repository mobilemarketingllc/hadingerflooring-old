<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.css");
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);

    register_shortcodes();
});



// Remove Emoji
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'gallery-menu' => __( 'Gallery Menu' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


//* Remove Query String from Static Resources
function remove_css_js_ver( $src ) {
    if( strpos( $src, '?ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'remove_css_js_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_css_js_ver', 10, 2 );


// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


function register_shortcodes(){
    $dir=dirname(__FILE__)."/shortcodes";
    $files=scandir($dir);
    foreach($files as $k=>$v){
        $file=$dir."/".$v;
        if(strstr($file,".php")){
            $shortcode=substr($v,0,-4);
            add_shortcode($shortcode,function($attr,$content,$tag){
                ob_start();
                include(dirname(__FILE__)."/shortcodes/".$tag.".php");
                $c=ob_get_clean();
                return $c;
            });
        }
    }
}

/* ACF add options page */
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page( array(

        'page_title'    => 'Theme Settings',
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-settings',
        'capability'    => 'edit_posts',
        'icon_url' => 'dashicons-images-alt2',
        'position' => 65

    ) );

}

// Facetwp results
add_filter( 'facetwp_result_count', function( $output, $params ) {
    //$output = $params['lower'] . '-' . $params['upper'] . ' of ' . $params['total'] . ' results';
    $output =  number_format($params['total']) . ' Products';
    return $output;
}, 10, 2 );

// Facetwp results pager
function my_facetwp_pager_html( $output, $params ) {
    $output = '';
    $page = $params['page'];
    $total_pages = $params['total_pages'];
    if ( $page > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page - 1) . '"><span class="pager-arrow"><</span></a>';
    }
    $output .= '<span class="pager-text">page ' . $page . ' of ' . $total_pages . '</span>';
    if ( $page < $total_pages && $total_pages > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page + 1) . '"><span class="pager-arrow">></span></a>';
    }
    return $output;
}

add_filter( 'facetwp_pager_html', 'my_facetwp_pager_html', 10, 2 );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');

//Gravity Scroll to confirmation
add_filter( 'gform_confirmation_anchor', '__return_true' );


// Custom post types
function cptui_register_my_cpts_carpeting() {

    /**
     * Post Type: Carpeting.
     */

    $labels = array(
        "name" => __( 'Carpeting', '' ),
        "singular_name" => __( 'Carpeting', '' ),
        "menu_name" => __( 'Carpeting Catalog', '' ),
    );

    $args = array(
        "label" => __( 'Carpeting', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "flooring/carpet/carpet-catalog", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title" ),
    );

    register_post_type( "carpeting", $args );
}

add_action( 'init', 'cptui_register_my_cpts_carpeting' );

function cptui_register_my_cpts_hardwood() {

    /**
     * Post Type: Hardwood.
     */

    $labels = array(
        "name" => __( 'Hardwood', '' ),
        "singular_name" => __( 'Hardwood', '' ),
        "menu_name" => __( 'Hardwood Catalog', '' ),
    );

    $args = array(
        "label" => __( 'Hardwood', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "flooring/hardwood/hardwood-catalog", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title" ),
    );

    register_post_type( "hardwood", $args );
}

add_action( 'init', 'cptui_register_my_cpts_hardwood' );


function cptui_register_my_cpts_laminate() {

    /**
     * Post Type: Laminate.
     */

    $labels = array(
        "name" => __( 'Laminate', '' ),
        "singular_name" => __( 'Laminate', '' ),
        "menu_name" => __( 'Laminate Catalog', '' ),
    );

    $args = array(
        "label" => __( 'Laminate', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "flooring/laminate/laminate-catalog", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title" ),
    );

    register_post_type( "laminate", $args );
}

add_action( 'init', 'cptui_register_my_cpts_laminate' );



function cptui_register_my_cpts_luxury_vinyl_tile() {

    /**
     * Post Type: Luxury Vinyl Tile.
     */

    $labels = array(
        "name" => __( 'Luxury Vinyl Tile', '' ),
        "singular_name" => __( 'Luxury Vinyl Tile', '' ),
    );

    $args = array(
        "label" => __( 'Luxury Vinyl Tile', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "flooring/luxury-vinyl-tile/luxury-vinyl-tile-catalog", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title" ),
    );

    register_post_type( "luxury_vinyl_tile", $args );
}

add_action( 'init', 'cptui_register_my_cpts_luxury_vinyl_tile' );



function cptui_register_my_cpts_glass_tile() {

    /**
     * Post Type: Glass Tile.
     */

    $labels = array(
        "name" => __( 'Glass Tile', '' ),
        "singular_name" => __( 'Glass Tile', '' ),
        "menu_name" => __( 'Glass Tile Catalog', '' ),
    );

    $args = array(
        "label" => __( 'Glass Tile', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "flooring/glass-tile/glass-tile-catalog", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title" ),
    );

    register_post_type( "glass_tile", $args );
}

add_action( 'init', 'cptui_register_my_cpts_glass_tile' );



function cptui_register_my_cpts_natural_stone() {

    /**
     * Post Type: Natural Stone.
     */

    $labels = array(
        "name" => __( 'Natural Stone', '' ),
        "singular_name" => __( 'Natural Stone', '' ),
        "menu_name" => __( 'Natural Stone Catalog', '' ),
    );

    $args = array(
        "label" => __( 'Natural Stone', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "flooring/natural-stone/natural-stone-catalog", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title" ),
    );

    register_post_type( "natural_stone", $args );
}

add_action( 'init', 'cptui_register_my_cpts_natural_stone' );



//* Tile CPT */
function cptui_register_my_cpts_tile() {

    /**
     * Post Type: Tile.
     */

    $labels = array(
        "name" => __( 'Tile', '' ),
        "singular_name" => __( 'Tile', '' ),
        "menu_name" => __( 'Tile Catalog', '' ),
    );

    $args = array(
        "label" => __( 'Tile', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "flooring/tile-stone/tile-stone-catalog", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title" ),
    );

    register_post_type( "tile", $args );
}

add_action( 'init', 'cptui_register_my_cpts_tile' );



//Automatically registers new BB Modules from the theme
function register_custom_modules(){
    if ( class_exists( 'FLBuilder' ) ) {
        $dir=dirname(__FILE__)."/fl-builder/custom-modules";
        $files=scandir($dir);
        foreach($files as $k=>$v){
            $file=$dir."/".$v;
            if(is_dir($file) && file_exists($file."/index.php")){
                include_once($file."/index.php");
            }
        }
    }
}

add_action("init","register_custom_modules");

add_image_size("logo_slider",false,52);
function new_year_number() 
{
	return $new_year = date('Y');
}
add_shortcode('year_code_4', 'new_year_number');

function new_year_number_2() 
{
	return $new_year = date('y');
}
add_shortcode('year_code_2', 'new_year_number_2');




if (!function_exists('write_log')) {
    function write_log($log) {
        if (is_array($log) || is_object($log)) {
            error_log(print_r($log, true));
        } else {
            error_log($log);
        }
    }
}


//Roomvo script
add_action('wp_footer', 'roomvo_custom_footer_js');
function roomvo_custom_footer_js() {
    
  echo "<script src='https://www.roomvo.com/static/scripts/b2b/mobilemarketing.js' async></script>";
}


//Roomvo script integration
function roomvo_script_integration($manufacturer,$sku){

    
         $manufacturer = str_replace(' ', '', strtolower($manufacturer));  

                $content='<div class="roomvo-container">
                                       <a class="roomvo-stimr fl-button" data-sku="'.$sku.'"
                                       style="visibility: hidden"><span class="fl-button-text">See This In My Room</span></a>
                          </div>';
       
               $content .='<script type="text/javascript">
                               function getProductSKU() {                       
                               return "'.$manufacturer.'-'.$sku.'";
                               }
                           </script>';
        
                 echo $content;
    
   
}


// wp_clear_scheduled_hook( 'roomvo_csv_integration_cronjob_great' );
// if (!wp_next_scheduled('roomvo_csv_integration_cronjob_hadingerflooring')) {    
      
//     wp_schedule_event( time() +  20800, 'daily', 'roomvo_csv_integration_cronjob_hadingerflooring');
// }
// add_action( 'roomvo_csv_integration_cronjob_hadingerflooring', 'roomvo_csv_integration' );


//Roomvo csv integration
function roomvo_csv_integration(){

    write_log('in roomvo_csv_integration');
    $data = array();
    
    $brandmapping = array(     
        "hardwood",
        "laminate",
        "luxury_vinyl_tile",
        "glass_tile",
        "natural_stone",
        "tile",
        "carpeting"
    );      

            $protocols = array('https://', 'https://www.', 'www.','http://', 'http://www.');
            $domain = str_replace($protocols, '', home_url());

            $upload_dir = wp_get_upload_dir();
            $file= $upload_dir['basedir']. '/sfn-data/product_file.csv';

            write_log($file);

            $file = fopen($file, 'w');
            
            // save the column headers
            fputcsv($file, array('Manufacturer', 'Manufacturer SKU Number', 'product page URL', 'Dealer name', 'Dealer website domain'));
            
            foreach($brandmapping as $product_post){

                write_log('<----------'.$product_post.' Started--------->');
                sleep(20);

            // Sample data. This can be fetched from mysql too

            global $wpdb;
            $product_table = $wpdb->prefix."posts";
            $products_data = $wpdb->get_results("SELECT ID FROM $product_table WHERE post_type = '$product_post' AND post_status = 'publish'");


                    $i =1;
                    foreach($products_data as $product) {

                      //  if(get_post_meta( $product->ID, 'brand', true ) != 'Shaw'){ 

                                if(get_post_meta( $product->ID, 'brand', true ) == 'Bruce'){ 
                                    $manufacturer = get_post_meta( $product->ID, 'brand', true );
                                    }
                                    elseif(get_post_meta( $product->ID, 'manufacturer', true ) == 'AHF'){
                                        $manufacturer = get_post_meta( $product->ID, 'brand', true );
                                    }
                                    elseif(get_post_meta( $product->ID, 'manufacturer', true ) == 'Shaw'){
                                        $manufacturer = get_post_meta( $product->ID, 'brand', true );
                                    }
                                    else{
                                        $manufacturer = get_post_meta( $product->ID, 'manufacturer', true );
                                    }

                                    $sku = get_post_meta( $product->ID, 'sku', true );
                                    $product_url = get_permalink( $product->ID );
                                    $dealer_name = 'Hadinger Flooring';
                                    $domain_url = $domain; 

                                    fputcsv($file, array($manufacturer, $sku, $product_url, $dealer_name, $domain_url));

                                    if($i % 5000==0){ 
                                        write_log($i.'----5000'); 
                                        sleep(10);
                                    }

                                    $i++;

                       // }
                        
                    }

            wp_reset_query();

            write_log('<----------'.$product_post.' Ended--------->');

            }
            
            $file_size =  $upload_dir['basedir']. '/sfn-data/product_file.csv' ;

            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.roomvo.com/pro/api/ext/update_dealer_mappings",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array(
                'requester' => 'mobilemarketing',
                'api_key' => 'cac7fb3c-7996-499d-8729-96a3547515b8',
                'dealer_name' => 'Hadinger Flooring',
                'dealer_domain' => $domain,
                'email' => 'devteam.agency@gmail.com',
                'product_file' => new CURLFILE($file_size)
                ),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: multipart/form-data",
                "Content-Type: application/json"
            ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            write_log('<----------response--------->');
            write_log( $response);
            write_log('<----------response--------->');

}



function get_image_url ($image,$height,$width){

    if(strpos($image, 'mmllc-images.s3') !== false || strpos($image, 'products.daltile.com') !== false || strpos($image, 's7.shawimg.com') !== false){

        
        if(strpos($image , 's7.shawimg.com') !== false){
            if(strpos($image , 'http') === false){ 
            $image = "http://" . $image;
        }	
           
        }else{
            if(strpos($image , 'http') === false){ 
            $image = "https://" . $image;
        }	
            $image= "https://mm-media-res.cloudinary.com/image/fetch/h_".$height.",w_".$width.",c_limit/".$image."";
        }
    
      }else{
    
        $image= 'https://res.cloudinary.com/mobilemarketing/image/upload/c_limit,h_'.$height.',w_'.$width.',q_auto'.$image;
    
      }	

      return $image;

}
