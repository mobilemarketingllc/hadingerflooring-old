<?php
        $collection = get_field('collection');        
        $image = swatch_image_product(get_the_ID(),'600','400');
      //  $image_thumb = swatch_image_product_thumbnail(get_the_ID(),'222');
?>
<div id="product-images-holder"  <?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall'  || $collection == 'Floorte Magnificent') { ?>class="colorwall-exlusive-batch"<?php } ?>>
    <?php  
        if (!empty($image)){
    ?>
    <div class="img-responsive toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: 100% 100%;background-position:center" data-targetimg="gallery_item_0" data-responsive="<?php echo $image; ?>" data-src="<?php echo $image; ?>" data-exthumbimage="<?php echo $image; ?>">
        <a href="javascript:void(0)" class="popup-overlay-link"></a>
        <span class="main-imgs"><img src="<?php echo $image; ?>" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" /></span>
    </div>
    <?php } else{ ?>
    <div class="img-responsive toggle-image" data-targetimg="gallery_item_0" style="background-image:url('http://placehold.it/168x123?text=COMING+SOON');background-size: cover;background-position:center">
        <a href="http://placehold.it/168x123?text=COMING+SOON" class="popup-overlay-link"></a>
        <span class="main-imgs"><img src="http://placehold.it/168x123?text=COMING+SOON" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" /></span>
    </div>
    <?php } ?>
    <?php
        // check if the repeater field has rows of data
        if(get_field('gallery_room_images')){
            $gal_count = 1;
            $gallery_images = get_field('gallery_room_images');
            $gallery_img = explode("|",$gallery_images);
            // loop through the rows of data
            foreach($gallery_img as  $key=>$value) {

                $room_image =  high_gallery_images_in_loop($value);
                $room_image_thumb =  thumb_gallery_images_in_loop($value);

               
                
        ?>
            <div class="popup-imgs-holder" data-targetimg="gallery_item_<?php echo $gal_count; ?>" data-responsive="<?php echo $room_image; ?>" data-exthumbimage="<?php echo $room_image_thumb; ?>" data-src="<?php echo $room_image; ?>"><a href="javascript:void(0)" title="<?php the_title_attribute(); ?>"><span class="main-imgs"><img src="<?php echo $room_image; ?>" alt="<?php the_title_attribute(); ?>" /></span></a></div>
        <?php
            $gal_count++;
            }
        }
        ?>
</div>
    <?php if(get_field('gallery_room_images')){ ?>
    <div class="toggle-image-thumbnails <?php if($LAYOUT_COL == 5) : echo "vertical-slider"; endif; ?>">
        <?php if (!empty($image)){ ?>
            <div class="toggle-image-holder"><a href="javascript:void(0)" class="active" data-targetimg="gallery_item_0" data-background="<?php echo $image; ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a></div>
        <?php } ?>       
        <?php
        // check if the repeater field has rows of data
        if(get_field('gallery_room_images')){
            $gallery_count = 1;
            $gallery_images = get_field('gallery_room_images');
            $gallery_img = explode("|",$gallery_images);
            // loop through the rows of data
            foreach($gallery_img as  $key=>$value) {

                $room_image =  high_gallery_images_in_loop($value);
                $room_image_small =  thumb_gallery_images_in_loop($value);

        ?>
            <div class="toggle-image-holder">
                <a href="javascript:void(0)" data-targetimg="gallery_item_<?php echo $gallery_count; ?>" data-background="<?php echo $room_image; ?>" data-thumbnail="<?php echo $room_image_small; ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $room_image_small; ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a>
            </div>
        <?php
            $gallery_count++;
            }
          }
        ?>
    </div>
<?php } ?>