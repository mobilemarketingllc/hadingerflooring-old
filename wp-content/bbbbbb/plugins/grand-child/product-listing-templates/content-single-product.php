<?php
global $post;
$flooringtype = $post->post_type; 
$brand = get_field('brand') ;
$sku = get_field('sku') ;

$room_image =  single_gallery_image_product(get_the_ID(),'1000','1600');

$image_600 = swatch_image_product(get_the_ID(),'600','400');
		
?>
</div></div></div>
<style type="text/css">
.breadcrumbs{ display:none;}
</style>
<div class="product-banner-img" <?php if($room_image) {	?>style="background-image:url('<?php echo $room_image; ?>');"<?php } ?>></div>
<div class="container">
	<div class="row">
		<div class="fl-content product col-sm-12">
			<div class="product-detail-layout-default">
                <article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>">
                    <div class="fl-post-content clearfix grey-back" itemprop="text">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 product-swatch">
                                <div class="product-swatch-inner">
                                    <?php 
                                        $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-images.php';
                                        include( $dir );
                                    ?>
                                        <div class="clearfix"></div>
                                    <div class=" button-wrapper-default">                                        
										<?php if( get_option('getcouponbtn') == 1){  ?>
											<a href="<?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplaceurl');}else{ echo '/flooring-coupon/'; } ?>" target="_self" class="button alt getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
											<?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplacetext');}else{ echo 'GET COUPON'; }?>
											</a>
            							<?php } ?>
                                        <a href="/contact-us/" class="button contact-btn">CONTACT US</a>
										<?php if(get_option('pdp_get_finance') != 1 || get_option('pdp_get_finance') == '' ){?>						
											<a href="<?php if(get_option('getfinancereplace')==1){ echo get_option('getfinancereplaceurl');}else{ echo '/flooring-financing/'; } ?>" class="finance-btn button"><?php if(get_option('getfinancereplace')=='1'){ echo get_option('getfinancetext');}else{ echo 'Get Financing'; } ?></a>	
										<?php } ?>
                                    </div>
                                </div>
                            </div>
                                
                            <div class="col-md-5 col-sm-12 col-md-offset-1 product-box pdp2-leftbox">
                                <div class="row">
								<div class="col-md-6">
                                        <?php if(get_field('parent_collection')) { ?>
                                        <h4>
                                            <?php the_field('parent_collection'); ?> </h4>
                                        <?php } ?>
                                        <h2 class="fl-post-title" itemprop="name">
                                            <?php the_field('collection'); ?>
                                        </h2>
                                        <h1 class="fl-post-title" itemprop="name">
                                            <?php the_field('color'); ?>
                                        </h1>
                                        <?php /*?><h5>Style No. <?php the_field('style'); ?>, Color No. <?php the_field('color_no'); ?></h5><?php*/ ?>
                                    </div>

                                    <div class="product-colors col-md-6">
										<?php
										    $collection = get_field('collection', $post->ID);
                                            $familysku = get_post_meta($post->ID, 'collection', true);     
											$familycolor = get_post_meta($post->ID, 'style', true);                  
				
											if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { 
				
											$args = array(
												'post_type'      => $flooringtype,
												'posts_per_page' => -1,
												'post_status'    => 'publish',
												'meta_query'     => array(
													array(
														'key'     => 'style',
														'value'   => $familycolor,
														'compare' => '='
													),
													array(
														'key' => 'swatch_image_link',
														'value' => '',
														'compare' => '!='
														)
												)
											);
										}else{
				
											$args = array(
												'post_type'      => $flooringtype,
												'posts_per_page' => -1,
												'post_status'    => 'publish',
												'meta_query'     => array(
													array(
														'key'     => 'collection',
														'value'   => $familysku,
														'compare' => '='
													),
													array(
														'key' => 'swatch_image_link',
														'value' => '',
														'compare' => '!='
														)
												)
											);
										}
											$the_query = new WP_Query( $args );
                                            
                                        ?>
                                        <?php
                                            $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-brand-logos.php';
                                            include_once( $dir );      
											?>
                                    </div>
									<div class="product-colors col-md-12">
										<?php ?>
											<ul>
												<li class="color-count" style="font-size:14px;"><?php echo $the_query->found_posts; ?> Colors Available</li>
											</ul>
										<?php ?>
									</div>
                                    
                                    
                                        <div class="clearfix"></div>
                                </div>
									<div class="clearfix"></div>
									
<!-- Product Slider Start -->

									<div class="product-variations">
										<!-- <h3>Color Variations</h3> -->
										<?php
										$familysku = get_post_meta($post->ID, 'collection', true);

										$args = array(
											'post_type'      => $flooringtype,
											'posts_per_page' => -1,
											'post_status'    => 'publish',
											'meta_query'     => array(
												array(
													'key'     => 'collection',
													'value'   => $familysku,
													'compare' => '='
												),
												array(
													'key' => 'swatch_image_link',
													'value' => '',
													'compare' => '!='
													)
											)
										);
										?>
										<?php
										$the_query = new WP_Query($args);
										?>
										<div class="color_variations_slider_default">
											<div class="slides">
												<?php
													while ($the_query->have_posts()) {
													$the_query->the_post();

													$image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
													
													$style = "padding: 5px;";
												?>
												<div class="slide col-md-3 col-sm-3 col-xs-6 color-box">
													<figure class="color-boxs-inner">
														<div class="color-boxs-inners">
															<a href="<?php the_permalink(); ?>">
																<img src="<?php echo $image; ?>" style="<?php echo $style; ?>" class="swatch-img tooltipped" data-position="top" data-delay="50" data-tooltip="<?php the_title(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" width="100" height="100" />
															</a>
															<br />
															<small><?php the_field('color'); ?></small>
														</div>
													</figure>
												</div>
												<?php
													} 										
													wp_reset_postdata();
												?>
											</div>
										</div>
									</div>
<!-- Product Slider End -->	
										<div class="clearfix"></div>
									<div class="responsive-button-wrapper-default">
									<?php if( get_option('getcouponbtn') == 1){  ?>
											<a href="<?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplaceurl');}else{ echo '/flooring-coupon/'; } ?>" target="_self" class="button alt getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
											<?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplacetext');}else{ echo 'GET COUPON'; }?>
											</a>
            						<?php } ?>
                                        <a href="/contact-us/" class="button contact-btn">CONTACT US</a>
										<?php if(get_option('pdp_get_finance') != 1 || get_option('pdp_get_finance') == '' ){?>						
											<a href="<?php if(get_option('getfinancereplace')==1){ echo get_option('getfinancereplaceurl');}else{ echo '/flooring-financing/'; } ?>" class="finance-btn button"><?php if(get_option('getfinancereplace')=='1'){ echo get_option('getfinancetext');}else{ echo 'Get Financing'; } ?></a>	
										<?php } ?>			                                        
                                    </div>


                            <div class="clearfix"></div>
                                <div id="product-attributes-wrap">
                                    <?php 
                                        $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-attributes.php';
                                        include( $dir );
                                    ?>
                                </div>
                            </div>
                                <div class="clearfix"></div>
                        </div>
                            <div class="clearfix"></div>
                        
                    </div>
                </article>
            </div>
			<?php
			$title = get_the_title();

			$jsonld = array('@context'=>'https://schema.org/','@type'=>'Product','name'=> $title,'image'=>$image_600,'description'=>$title,'sku'=>$sku,'mpn'=>$sku,'brand'=>array('@type'=>'thing','name'=>$brand), 
			'offers'=>array('@type'=>'offer','priceCurrency'=>'USD','price'=>'00','priceValidUntil'=>''));
			?>
			<?php echo '<script type="application/ld+json">'.json_encode($jsonld).'</script>';	?>